# DATA304/COMP312 - Group Project

This GitLab has been created to store all of Group 7's Data 304/Comp 312 Group project files in a way accessible to all members of the group. 

This repository contains all progress reports, code and data used to conduct our study on "The Lab Cafe" queueing system.

## Project members

| Name             | Email                       | ORCID ID             |
| ---------------- | --------------------------  | -------------------- |
| Rebecca Robson   | robsonrebe@myvuw.ac.nz      | 0009-0000-1949-1012  |
| Olivia Baird     | bairdol@myvuw.ac.nz         | 0000-0003-1275-8858  |
| Ray Wang         | wangzhir@myvuw.ac.nz        | 0009-0005-3779-1947  |
| Quang Thinh Lam  | lamthin@myvuw.ac.nz         | 0009-0008-0043-2950  |
| Riya Rana        | ranariya@myvuw.ac.nz        | 0009-0004-2266-3734  |